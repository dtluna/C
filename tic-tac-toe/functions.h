//Подключаем необходимые нам библиотеки. Почти всегда нужны stdio.h и stdlib.h. 
//Обычно всё #include для библиотек, лежащих в системе, пишут в заголовочники.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
//библиотека для работы со строками
void show_board (char board[]);/*Прототип функции, также известный как объявление (declaration).
Нужен, чтобы компилятор знал, что такой функция будет дано определение(definition) позже.
Попробуешь вызвать функцию до её объявления - конпелятор обматерит.
Это нужно для контроля компилятора правильности вызова функции (передачи аргументов, возвращаемого значения, etc.)
Промеж скобочек указываем аргументы. Их могёт и не быть. Здесь есть строчка board.*/
char* player_turn (char player_char, char board[]);
char select_player_char (char message[]);
_Bool check_victory (char board[]);
_Bool check_draw(char board[]);