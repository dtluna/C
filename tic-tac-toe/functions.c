#include "functions.h"

#ifndef REMOVE_NEWLINE
#define REMOVE_NEWLINE while(getchar() == '\n') continue;	//а это макрос
#endif

//хорошим тоном считается называть файлы с кодом функций и заголовочные файлы с объявлениями этих функций одинаковыми именами
//В данном случае файл с кодом функций - function.c, а заголовочный к нему - functions.h
void
show_board (char board[])	//а вот это уже определение функции
{
  /*printf() - функция из stdio.h, предназначенная для вывода инфы на консольку.
     У неё есть всякие там форматы вывода. Например '%d'(decimal) - вывод целого числа в десятичной форме,
     а %C (character) - вывод символа.
     Я не справочник, так что man printf. */
  printf ("%C | %C | %C\n", board[0], board[1], board[2]);	/*нумерация в массивах начинается с нуля. 
								   board[0] - нулевой элемент массива board */
  puts ("----------");
  printf ("%C | %C | %C\n", board[3], board[4], board[5]);
  puts ("----------");
  printf ("%C | %C | %C\n", board[6], board[7], board[8]);
}

char *
player_turn (char player_char, char board[])
{
  while (1)			//бесконечный цикл
    {
      printf ("Player '%C' turn!\n", player_char);
      show_board (board);
      puts ("Select spot(1-9):");
      //REMOVE_NEWLINE
      int pos = 0;
      scanf ("%d", &pos);	//scanf() - функция чтения чего-либо из потока ввода. man scanf

      /*вызов функции show_board(), определённой выше. 
         board - наша строчка, которую мы передаём первым аргументом. */
      if (pos < 1 || pos > 9)	//'||' - логическое или
	{
	  fprintf (stderr,
		   "Invalid input: %C!\nThe positions lie within [1,9] range!\n",
		   pos);
	  //функция вывода в файл. В данном случае - стандартный поток вывода ошибок stderr
	  continue;		//переход к следующей итерации цикла
	}
      else if (board[pos - 1] == pos + '0' /*получаем число из символа */ )	//если позиция не занята
	{
	  board[pos - 1] = player_char;
	  break;		//вызод из цикла
	}
      else
	{
	  fprintf (stderr, "This spot is taken!\n");
	  continue;
	}
    }

  if (check_victory (board))
    {
      printf ("Player %C wins!\n", player_char);
      show_board (board);
      _exit (EXIT_SUCCESS);	//выход из программы
    }

  if (check_draw (board))
    {
      printf ("Draw!\n", player_char);
      show_board (board);
      _exit (EXIT_SUCCESS);	//выход из программы
    }

  return board;
}

char
select_player_char (char message[])
{
  puts (message);
  char c = getchar ();		//функция считывания символа из потока ввода
  while ('x' != c && 'o' != c)	//!= - не равно, && - логическое И
    {
      fprintf (stderr, "Invalid input!\nYou can only enter 'x' or 'o'!\n%s\n",
	       message);
      c = getchar ();
    }
  return c;
}

_Bool
check_victory (char board[])
{
  return ((board[0] == board[1] == board[2])
	  || (board[3] == board[4] == board[5])
	  || (board[6] == board[7] == board[8])
	  || (board[0] == board[3] == board[5])
	  || (board[1] == board[4] == board[7])
	  || (board[2] == board[5] == board[8])
	  || (board[0] == board[4] == board[8])
	  || (board[2] == board[4] == board[6]));
  //страшно, правда?
}

_Bool
check_draw (char board[])
{
  int cells_left = 9;
  for (int i = 0; i < 9; ++i)	//++ - увеличить на 1
    if ('x' == board[i] || 'o' == board[i])
      cells_left--;		//-- - уменьшить на 1 

  if (cells_left < 1)
    return true;
  else
    return false;
}
