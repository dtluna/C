#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char const *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage:%s <filename>\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  FILE *file = fopen (argv[1], "r");


  if (NULL == file)
    {
      perror ("fopen");
      exit (EXIT_FAILURE);
    }

  puts ("Press return.");
  getchar ();

  fclose (file);

  return 0;
}
