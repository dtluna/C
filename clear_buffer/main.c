#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 4096

void
clear_buffer (char *buffer)
{
  for (int i = 0; i < BUF_SIZE; ++i)
    {
      buffer[i] = 0;
    }
}

int
main (int argc, char const *argv[])
{
  char buffer[BUF_SIZE] = "One vision! One purpose!";
  puts (buffer);
  clear_buffer (buffer);
  puts (buffer);
  return 0;
}
