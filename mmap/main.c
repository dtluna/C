#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "getch.h"

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

#ifndef MiB
#define MiB(number) 1024*1024*number
#endif

int
main (int argc, const char **argv)
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage:%s <amount of MiBs to allocate>.\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  size_t nmemb = MiB (atoll (argv[1]));
  printf ("Will allocate %ld bytes (%d MiB).\n", nmemb, atoi (argv[1]));
  char *array = (char *) mmap (NULL, nmemb, PROT_READ | PROT_WRITE,
			       MAP_PRIVATE | MAP_ANONYMOUS, NULL, NULL);
  if (MAP_FAILED == array)
    handle_error ("mmap");
  for (long i = 0; i < nmemb; ++i)
    array[i] = i + 1;
  puts ("Allocated.\nPress any key to free.");
  printf ("Array adress = %p\n", array);
  getch ();
  munmap (array, nmemb);
  printf ("Array adress = %p\n", array);
  puts ("Freed.\nPress any key to continue.");
  getch ();
  return 0;
}
