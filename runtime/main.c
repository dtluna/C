#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int
main (int argc, char const *argv[])
{
  time_t run_time = 0;
  time_t uptime = 0;
  time_t start_time = 0;
  clock_t start_time_clk = 0;

  FILE *uptime_file = NULL;

  FILE *stat_file = fopen ("/proc/self/stat", "r");
  fscanf (stat_file,
	  "%*d %*s %*c %*d %*d"
	  "%*d %*d %*u %*u %*lu"
	  "%*lu %*lu %*lu %*lu %*lu"
	  "%*ld %*ld %*ld %*ld %*ld" "%*ld %lu", &start_time_clk);
  start_time = start_time_clk / sysconf (_SC_CLK_TCK);
  fclose (stat_file);

  while (1)
    {
      uptime_file = fopen ("/proc/uptime", "r");
      fscanf (uptime_file, "%lu", &uptime);
      fclose (uptime_file);

      //printf("uptime = %lu\n", uptime);
      //printf("start_time = %lu\n", start_time);
      run_time = uptime - start_time;
      printf ("run_time = %lu\n", run_time);
      puts ("----");

      sleep (1);
    }

  return 0;
}
