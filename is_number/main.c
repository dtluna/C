#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

typedef char bool;
#define false 0
#define true 1

void NULL_error (int return_value, char *message);
#define fopen_err(e) NULL_error(e, "fopen");
#define malloc_err(e) NULL_error(e,"malloc");

bool is_number (const char *string);
char *alloc_str (size_t length);

void
NULL_error (int return_value, char *message)
{
  if (NULL == return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}

char *
alloc_str (size_t length)
{
  char *string = (char *) calloc (length, sizeof (char));
  if (NULL == string)
    malloc_err (string);
  return string;
}

bool
is_number (const char *string)
{
  char *endptr = alloc_str (strlen (string));
  long int number = strtol (string, &endptr, 10);
  if ((0 == number) && (0 == strcmp (string, endptr)))
    return false;
  else
    return true;
}

int
main (int argc, char const *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage:%s <number> [numbers...]\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  for (size_t i = 1; i < argc; ++i)
    {
      if (is_number (argv[i]))
	printf ("%s is a decimal number, indeed!\n", argv[i]);
      else
	printf ("%s isn't a decimal number, no.\n", argv[i]);
    }

  return 0;
}
