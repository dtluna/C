#include <stdio.h>
#include <stdlib.h>

int
main ()
{
  int *p = NULL;
  p = (int *) calloc (1, sizeof (int));
  printf ("p = %p\n", p);
  free (p);
  printf ("p = %p\n", p);
  return 0;
}
