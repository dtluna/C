#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char const *argv[])
{
  char *string = (char *) calloc (256, sizeof (char));
  int i;
  puts ("Enter an integer:");
  scanf ("%d", &i);

  sprintf (string, "%d", i);
  puts (string);
  return 0;
}
