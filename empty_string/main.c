#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef empty_string
#define empty_string(string, size) strncpy(string, "", size)
#endif

int
main (int argc, char const *argv[])
{
  char string[256];

  empty_string (string, 256);

  for (int i = 0; i < 256; ++i)
    {
      printf ("%d\n", string[i]);
    }

  return 0;
}
