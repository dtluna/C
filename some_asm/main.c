#include <math.h>
#include <stdio.h>
#include <time.h>
//F = sin(x)/(x^2 + 10)

int
main (int argc, char const *argv[])
{
  clock_t start, end;
  clock_t worktime;
  int C1 = 10;
  long double res = 0, s;
  double min, max;

  puts ("Enter minimal number:");
  scanf ("%lf", &min);
  puts ("Enter maximal number:");
  scanf ("%lf", &max);


  start = clock ();
  for (double x = min; x < max; x += 0.01)
    {
      res = (sin (x)) / (pow (x, 2) + 10.0);
    }
  printf ("%LF\n", res);
  end = clock ();
  float timec = (end - start);
  printf ("Time in C = %f\n", timec);

  start = clock ();
  for (double x = min; x <= max; x += 0.01)
    {
      __asm__ __volatile__ ("fild %0\n"
			    "fsin\n"
			    "fld %0\n"
			    "fmul %0\n"
			    "fadd %2\n"
			    "fstpl %1\n"
			    "fdiv %1\n"
			    "fstpl %1\n":"+m" (x), "+m" (s), "+m" (C1));

    }
  printf ("%LF\n", res);
  end = clock ();
  float timeasm = end - start;
  printf ("Time in asm = %f\n", timeasm);

  return 0;
}
