#include <stdio.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

typedef char bool;
#define true 1
#define false 0

void check_error (int return_value, int error_value, char *message);
struct dirent **get_namelist ();
int filter_dir (const struct dirent *entry);
bool is_number (const char *string);
char *alloc_str (size_t length);

#define NULL_error(e, msg) check_error(e, NULL, msg);
#define fopen_err(e) NULL_error(e, "fopen");
#define malloc_err(e) NULL_error(e,"malloc");

#define minus_one_error(e, msg) check_error(e, -1, msg);
#define chdir_err(e) minus_one_error(e,"chdir");
#define scandir_err(e) minus_one_error(e,"scandir");

char *
alloc_str (size_t length)
{
  char *string = (char *) calloc (length, sizeof (char));
  malloc_err (string);
  return string;
}


void
check_error (int return_value, int error_value, char *message)
{
  if (error_value == return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}

int
pid_dir_compare (const struct dirent **first_entry,
		 const struct dirent **second_entry)
{
  pid_t first_pid = atoi ((*first_entry)->d_name);
  pid_t second_pid = atoi ((*second_entry)->d_name);
  return (first_pid - second_pid);
}

struct dirent **
get_namelist ()
{
  struct dirent **namelist = NULL;
  scandir_err (scandir (".", &namelist, &filter_dir, &pid_dir_compare));
  return namelist;
}

int
filter_dir (const struct dirent *entry)
{
  if ((DT_DIR == entry->d_type) && (is_number (entry->d_name)))
    return 1;
  else
    return 0;
}

bool
is_number (const char *string)
{
  char *endptr = alloc_str (strlen (string));
  long int number = strtol (string, &endptr, 10);
  if ((0 == number) && (0 == strcmp (string, endptr)))
    return false;
  else
    return true;
}

int
main (int argc, const char *argv[])
{
  if (argc < 2 || 2 < argc)
    {
      fprintf (stderr, "Usage:%s <directory>\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  chdir_err (chdir (argv[1]));
  struct dirent **namelist = get_namelist ();
  size_t i = 0;

  while (NULL != namelist[i])
    {
      puts (namelist[i]->d_name);
      i++;
    }
  free (namelist);

  return 0;
}
