#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

void
check_error (int return_value, int error_value, char *message)
{
  if (error_value == return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}

#define NULL_error(e, msg) check_error(e, NULL, msg);
#define fopen_err(e) NULL_error(e, "fopen");

#define EOF_error(e, msg) check_error(e, EOF, msg);
#define fclose_err(e) EOF_error(e, "fclose");
#define fscanf_err(e) EOF_error(e, "fscanf");

clock_t
get_cpu_time ()
{
  FILE *cpu_stat_file = fopen ("/proc/stat", "r");
  fopen_err (cpu_stat_file);

  clock_t user = 0, nice = 0, system_time = 0, idle = 0, iowait = 0;
  clock_t irq = 0, softirq = 0, steal = 0, guest = 0, guest_nice = 0;

  fscanf (cpu_stat_file,
	  "%*s %lu %lu %lu %lu %lu"
	  "%lu %lu %lu %lu %lu",
	  &user, &nice, &system_time, &idle, &iowait,
	  &irq, &softirq, &steal, &guest, &guest_nice);

  clock_t cpu_time =
    user + nice + system_time + idle + iowait + irq + softirq + steal +
    guest + guest_nice;

  fclose_err (fclose (cpu_stat_file));

  return cpu_time;
}



int
main (int argc, char const *argv[])
{
  if (argc < 3)
    {
      fprintf (stderr, "Usage:%s <number of nanoseconds to sleep> <pid>\n",
	       argv[0]);
      exit (EXIT_FAILURE);
    }

  FILE *stat_file;
  clock_t utime = 0, stime = 0;
  clock_t proc_time = 0;;
  pid_t pid = 0;
  struct timespec ts;
  ts.tv_sec = 0;
  ts.tv_nsec = atoll (argv[1]);

  char *path = (char *) calloc (256, sizeof (char));
  sprintf (path, "/proc/%s/stat", argv[2]);
  time_t tse = 0;
  clock_t cpu_time = 0;
  double f_proc_time = 0.0;
  while (1)
    {
      stat_file = fopen (path, "r");
      fopen_err (stat_file);

      fscanf (stat_file,
	      "%d %*s %*c %*d %*d"
	      "%*d %*d %*u %*u %*lu"
	      "%*lu %*lu %lu %lu", &pid, &utime, &stime);
      cpu_time = get_cpu_time ();

      proc_time = utime + stime;
      f_proc_time = proc_time * 1.0;
      fclose (stat_file);
      tse = time (NULL);
      printf ("Time:%s", ctime (&tse));
      printf ("pid:%d\n"
	      "proc_time = %lu\n"
	      "cpu_time = %lu\n"
	      "%%CPU = %3.1f%%\n",
	      pid, proc_time, cpu_time, 100.0 * (f_proc_time / cpu_time));
      nanosleep (&ts, NULL);
    }


  return 0;
}
