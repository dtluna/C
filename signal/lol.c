#include <stdio.h>
#include <stdlib.h>
#include <linux/signal.h>
#include <sys/types.h>
#include <unistd.h>

void
quithandler (int signum)
{
  puts ("Caught signal SIGQUIT.");
}

void
handler (int signum)
{
  puts ("Caught signal SIGINT.\nStarting output.");
  for (int i = 0; i < 10; ++i)
    {
      sleep (1);
      printf ("%d", i);
      fflush (stdout);
    }
  puts ("");
}

int
main (int argc, char const *argv[])
{
  struct sigaction act;
  act.sa_handler = &handler;
  int signum = SIGINT;
  sigaction (signum, &act, NULL);
  /*signum = SIGQUIT;
     act.sa_handler = &quithandler;
     sigaction(signum, &act, NULL); */
  while (1)
    {

    }
  return 0;
}
