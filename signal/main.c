#include <stdio.h>
#include <stdlib.h>
#include <linux/signal.h>
#include <sys/types.h>
#include <unistd.h>

void
handler (int signum)
{
  printf ("#%d:", getpid ());
  puts ("Caught signal. Starting output.");
  for (int i = 0; i < 10; ++i)
    {
      sleep (1);
      printf ("%d", i);
      fflush (stdout);
    }
  puts ("");
}

int
main ()
{
  printf ("Parent process PID:%d\n", getpid ());

  struct sigaction act;
  act.sa_handler = &handler;
  sigaction (SIGUSR1, &act, NULL);	//ОБРАБОТЧИК ЗАПИЛИТЬ ГЛОБАЛЬНЫМ

  pid_t pid = fork ();
  if (pid)
    {
      int caught;
      sigset_t set;
      sigaddset (&set, SIGUSR1);
      sigwait (&set, &caught);
      printf ("%d\n", caught);
      puts ("That's all, folks!");
    }
  else
    {
      char c = 0;
      scanf (" %c", &c);
      kill (getppid (), SIGUSR1);
      scanf (" %c", &c);
    }
  return 0;
}
