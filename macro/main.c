#define kek(K) ((K)>0) ? (K) : -(K)

#include <stdio.h>

int
main ()
{
  int i = 0;			//is_int = 0;
  //puts("Enter an integer:");
  //is_int = scanf("%d ", &i);
  /*
     while(0 == is_int)
     {
     fprintf(stderr, "scanf: Invalid input (expected decimal integer).\n");
     //puts("Enter an integer:");
     is_int = scanf("%d ", &i);
     }
   */
  scanf ("%d", &i);
  printf ("%d\n", kek (i));
  return 0;
}
