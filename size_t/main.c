#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int
main (int argc, char const *argv[])
{
  //18446744073709551614L
  size_t a = (size_t) pow (2, pow (2, 8));


  printf ("sizeof(size_t) = %lu\n", sizeof (size_t));

  while (a++)
    {
      if (a == 0)
	printf ("max size_t value = %zu\n", a - 1);
    }
  return 0;
}
