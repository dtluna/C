#include <cdk/cdk.h>
#include <sys/stat.h>
#include "procinfo.h"

int
main (int argc, char const *argv[])
{
  char *filename = "./cmdline";
  FILE *cmdline_file = fopen (filename, "r");

  struct stat st;
  stat (filename, &st);
  size_t size = st.st_size;
  char *buffer = (char *) calloc (size, sizeof (char));
  fgets (buffer, size, cmdline_file);
  for (int i = 0; i < size - 1; ++i)
    if ('\0' == buffer[i])
      {
	buffer[i] = ' ';
      }
  puts (buffer);
  FILE *buffer_file = fopen ("./buffer", "w");
  for (int i = 0; i < size; ++i)
    {
      fprintf (buffer_file, "%c", buffer[i]);
    }
  return 0;
}
