#include <stdio.h>
#include <stdlib.h>
#include "my_malloc.h"

#define MEBIBYTE 1024*1024
#define true 1
#define false 0
typedef char bool;

char *
allocate_MiB (size_t nmemb)
{
  char *array = (char *) my_malloc (nmemb * MEBIBYTE);
  if (NULL == array)
    {
      perror ("my_malloc");
      exit (EXIT_FAILURE);
    }
  return array;
}

int
main (int argc, char const *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage:%s <amount of memory to allocate in MiB>.\n",
	       argv[0]);
      exit (EXIT_FAILURE);
    }

  size_t nmemb = atoi (argv[1]);
  if (0 == nmemb)
    {
      fprintf (stderr, "Anata baka?!\n");
      exit (EXIT_FAILURE);
    }

  puts ("If you want to quit, type 'q'.");
  puts ("If you want to free the memory, type 'f'.");
  puts ("If you want to allocate memory, type 'a'.");
  puts ("Author's notice: the program is smarter, than you think it is.");
  printf ("Specified memory size is %ld (0x%lX) MiB.\n", nmemb, nmemb);

  char *array = NULL;
  char c = 0;
  bool allocated = false;
  while ((c = getchar ()))
    {
      if ('q' == c)
	{
	  if (true == allocated)
	    my_free (array);
	  exit (EXIT_SUCCESS);
	}

      if (true == allocated)
	{
	  printf ("%ld (0x%lX) MiB allocated.\n", nmemb, nmemb);
	  fflush (stdout);

	  if ('f' == c)
	    {
	      my_free (array);
	      allocated = false;
	      puts ("Freed memory.");
	    }

	  if ('a' == c)
	    puts ("Memory is already allocated.");
	}
      else
	{
	  puts ("No memory allocated.");

	  if ('a' == c)
	    {
	      array = allocate_MiB (nmemb);
	      allocated = true;
	      printf ("Allocated %ld (0x%lX) MiB.\n", nmemb, nmemb);
	      fflush (stdout);
	    }

	  if ('f' == c)
	    puts ("Memory is already free.");
	}
    }

  return 0;
}
