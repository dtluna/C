#include <stdio.h>
#include <stdlib.h>
#include "my_malloc.h"
#include <time.h>

#define SIZE 1024*1024*128
#define true 1
#define false 0
typedef char bool;

int
main ()
{
  time_t start = time (NULL);
  long *array = (long *) my_malloc (SIZE * sizeof (long));
  for (long i = 0; i < SIZE; ++i)
    {
      array[i] = i + 1;
    }

  for (long i = 0; i < SIZE; ++i)
    {
      printf ("a[%ld]=%ld\n", i, array[i]);
    }
  my_free (array);
  time_t end = time (NULL);
  printf ("Running time: %ld s.\n", end - start);
  return 0;
}
