#ifndef MY_MALLOC_H
#define MY_MALLOC_H
#include <unistd.h>
/* Подключаем функцию sbrk */

struct mem_control_block {
	int is_available;
	int size;
};
void my_malloc_init();
void my_free(void *firstbyte); 
void *my_malloc(size_t numbytes); 
#endif
