#include "my_malloc.h"

int has_initialized = 0;
void *managed_memory_start;
void *last_valid_address;

void
my_malloc_init ()
{
/* Вытаскиваем последний валидный адрес из ОС */
  last_valid_address = (void *) sbrk (0);
/* Пока у нас еще нет памяти для управления,
 поэтому просто устанавливаем начало на last_valid_address*/
  managed_memory_start = last_valid_address;
/* мы все проинициализировали и готовы продолжать */
  has_initialized = 1;
}

void
my_free (void *firstbyte)
{
  struct mem_control_block *mcb;
/* По полученному указателю находим mem_control_block и восстанавливаем структуру*/
  mcb = firstbyte - sizeof (struct mem_control_block);
/* Помечаем блок как доступный (свободный) */
  mcb->is_available = 1;
/* Вот и все! Мы закончили. */
  return;
}

void *
my_malloc (size_t numbytes)
{
  /* Запоминаем текущую область памяти, где мы ищем */
  void *current_location;
  /* Это тоже самое, что и current_location, но располагается выше на величину memory_control_block */
  struct mem_control_block *current_location_mcb;
  /* Это место памяти мы вернем. Оно будет установлено в 0,
     пока мы не найдем подходящего размера памяти в доступной области */
  void *memory_location;
  /* Инициализируемся, если мы еще этого не сделали */
  if (!has_initialized)
    my_malloc_init ();
  /* Память, которую мы ищем, 
     должна включать память для хранения блока управления памятью (memory control block),
     но пользователям функции malloc не обязательно знать об этом, поэтому мы просто скрываем это от них. */
  numbytes = numbytes + sizeof (struct mem_control_block);
  /* Устанавливаем значение memory_location в 0, пока мы не найдем подходящего места (куска в памяти) */
  memory_location = 0;
  /* Начинаем искать с начала управляемой памяти. */
  current_location = managed_memory_start;
  /* Продолжаем это делать, пока не просмотрим все доступное пространство */
  while (current_location != last_valid_address)
    {
      /* current_location и current_location_mcb указывают на один и тот же адрес.
         Так как current_location_mcb имеет соответствующий тип, то мы можем его использовать в качестве структуры.
         current_location это свободный указатель (void pointer), поэтому мы можем его использовать,
         чтобы просчитать адресацию. */
      current_location_mcb = (struct mem_control_block *) current_location;
      if (current_location_mcb->is_available)
	{
	  if (current_location_mcb->size >= numbytes)
	    {
	      /* Ура! Мы нашли открытое (не занятое) место подходящего размера. */
	      /* Упс, теперь оно больше не свободно J */
	      current_location_mcb->is_available = 0;
	      /* Теперь мы его владельцы */
	      memory_location = current_location;
	      /* Выходим из цикла */
	      break;
	    }
	}
      /* Так как Текущий блок памяти не подходит, то переходим дальше */
      current_location = current_location + current_location_mcb->size;
    }
  /* Если нам все еще не удалось найти подходящее место в памяти,
     то мы просим у операционной системы еще памяти */
  if (!memory_location)
    {
      /* Сдвигаем границу на заданное число байт вверх */
      sbrk (numbytes);
      /* Новая память будет находится в месте, 
         где раньше находился последний валидный адрес (после выделения новой памяти он сместился). */
      memory_location = last_valid_address;
      /* Мы сдвигаем значение последнего валидного адреса на numbytes байт. */
      last_valid_address = last_valid_address + numbytes;
      /* Теперь нужно инициализировать mem_control_block */
      current_location_mcb = memory_location;
      current_location_mcb->is_available = 0;
      current_location_mcb->size = numbytes;
    }
  /* Теперь, в любом случае (ну за исключением, состояния ошибки), 
     memory_location адресует память, включая блок mem_control_block */
  /* Передвигаем указатель назад на размер структуры mem_control_block */
  memory_location = memory_location + sizeof (struct mem_control_block);
  /* Возвращаем указатель */
  return memory_location;
}
