#include <gc.h>
#include <assert.h>
#include <stdio.h>

int
main ()
{
  GC_INIT ();
  int *a = (int *) GC_MALLOC (5000000000);
  printf ("Heap size = %ld\n", GC_get_heap_size ());
  a = NULL;
  a = (int *) GC_REALLOC (a, 2 * sizeof (int));
  printf ("Heap size = %ld\n", GC_get_heap_size ());
  return 0;
}
