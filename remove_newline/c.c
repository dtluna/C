#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
remove_newline (char *string)
{
  int newline_pos = strlen (string) - 1;
  if ('\n' == string[newline_pos])
    string[newline_pos] = '\0';
}

void
get_string (char *string, size_t size)
{
  fgets (string, size, stdin);
  remove_newline (string);
  return;
}

int
main (int argc, char const *argv[])
{
  char string[256];
  get_string (string, 256);
  printf ("%s", string);
  return 0;
}
