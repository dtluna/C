#include <stdio.h>

int
main ()
{
  int count = 10;
  long store = 0;
  for (int i = 0; i < count; i++)
    {
      store += i;
      printf ("%ld\n", store);
    }

  return 0;
}
