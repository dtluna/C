#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <blkid/blkid.h>


int
main (int argc, char *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage: %s devname\n", argv[0]);
      exit (1);
    }

  blkid_probe pr = blkid_new_probe_from_filename (argv[1]);
  if (!pr)
    {
      err (1, "Failed to open %s", argv[1]);
      exit (1);
    }

  // Get number of partitions
  blkid_partlist ls;
  int nparts, i;

  ls = blkid_probe_get_partitions (pr);
  nparts = blkid_partlist_numof_partitions (ls);
  printf ("Number of partitions:%d\n", nparts);

  if (nparts <= 0)
    {
      printf ("Please enter correct device name! e.g. \"/dev/sdc\"\n");
      exit (1);
    }

  // Get UUID, label and type
  const char *uuid;
  const char *label;
  const char *type;
  char *dev_name = (char *) calloc (20, sizeof (char));
  for (i = 0; i < nparts; i++)
    {

      sprintf (dev_name, "%s%d", argv[1], (i + 1));

      pr = blkid_new_probe_from_filename (dev_name);
      blkid_do_probe (pr);

      blkid_probe_lookup_value (pr, "UUID", &uuid, NULL);

      if (-1 == blkid_probe_lookup_value (pr, "LABEL", &label, NULL))
        {
          err (1, "Failed to get label for %s", label);
        }

      blkid_probe_lookup_value (pr, "TYPE", &type, NULL);

      printf ("Name=%s, UUID=%s, LABEL=%s, TYPE=%s\n", dev_name, uuid, label,
	      type);

    }
  free(dev_name);
  blkid_free_probe (pr);

  return 0;
}
