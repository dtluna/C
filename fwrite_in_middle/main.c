#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char const *argv[])
{
  char string[16] = "Linux";
  FILE *file = fopen ("a", "w");
  fprintf (file, "%s", string);
  fclose (file);

  file = fopen ("a", "r+");
  if (-1 == fseek (file, 2, SEEK_SET))
    {
      perror ("fseek");
    }
  char c = 'z';
  fwrite (&c, sizeof (char), 1, file);

  rewind (file);
  fread (string, sizeof (char), 5, file);
  fclose (file);
  puts (string);

  return 0;
}
