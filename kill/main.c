#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>

int
main (int argc, char const *argv[])
{
  if (argc < 3)
    {
      fprintf (stderr, "Usage:%s <pid> <signal>\n", argv[0]);
      exit (-1);
    }

  pid_t pid = atoi (argv[1]);
  int signum = atoi (argv[2]);

  if (signum < 1)
    {
      fprintf (stderr, "Error: there is no such a signal!");
      exit (-1);
    }

  int ret_val = kill (pid, signum);
  if (-1 == ret_val)
    {
      perror ("kill");
      exit (-1);
    }

  return 0;
}
