#include <stdio.h>

struct time
{
  int hours;
  int minutes;
  int seconds;
};

typedef struct time time;

void
print_time (time t)
{
  printf ("Hours:%d, minutes:%d, seconds:%d\n", t.hours, t.minutes,
	  t.seconds);
}

int
main ()
{
  time noon = {.hours = 12,.minutes = 0,.seconds = 0 };
  print_time (noon);
  return 0;
}
