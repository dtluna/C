#include <stdio.h>
#include <stdlib.h>

int
main ()
{
  char *a = (char *) calloc (100000000000000, sizeof (char));
  if (NULL == a)
    {
      perror("calloc");
      exit(1);
    }
  for (size_t i = 0;; ++i)
    {
      a[i] = 10;
      printf ("%lu:%d\n", i, a[i]);
    }

  return 0;
}
