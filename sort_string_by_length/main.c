#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct str_info
{
  unsigned int pos[256];
  unsigned int len[256];
  unsigned int word_num;
};

struct str_info
initialise ()
{
  struct str_info info;

  info.word_num = 0;
  for (int i = 0; i < 256; i++)
    info.pos = info.len = 0;

  return info;
}

struct str_info
find_words (const char *string, unsigned int string_length)
{
  unsigned int i = 0;
  unsigned int curr_w_length = 0;

  struct str_info info;
  info = initialise ();

  while (i < string_length)
    {
      while (' ' == string[i] && i < string_length)
	{
	  i++;			//встали на первый символ слова
	}
      info.word_num++;
      info.pos[info.word_num - 1] = i;

      curr_w_length = 0;
      while (' ' != string[i + curr_w_length])
	curr_w_length++;	//нашли длину текущего слова
      info.len[info.word_num - 1] = curr_w_length;

      if (i < string_length)
	{
	  i += curr_w_length;
	}
    }
  return info;
}

char *
sort_string (char *string, unsigned int string_length)
{
  struct str_info info = find_words (string, string_length);



  return string;
}

int
main ()
{
  char *string = calloc (256, sizeof (char));

  puts ("Enter a string:");
  string = fgets (string, 256, stdin);

  string = sort_string (string, strlen (string) - 1);

  puts (string);

  free (string);
  return 0;
}
