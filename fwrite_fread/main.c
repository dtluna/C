#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct meh
{
  int a;
  char string[128];
  float pi;
};

typedef struct meh meh_t;

#ifndef meh_init
#define meh_init {.a = 0, .string = "", .pi = 3.141592}
#endif

int
main (int argc, char const *argv[])
{
  meh_t a = meh_init;
  a.a = 1488;
  strcpy (a.string, "linux");

  meh_t b = meh_init;
  b.a = 34;
  strcpy (b.string, "быдло!");

  FILE *file = fopen ("a", "w");
  fwrite (&a, sizeof (char), sizeof (meh_t), file);
  fwrite (&b, sizeof (char), sizeof (meh_t), file);
  fclose (file);

  meh_t c = meh_init;
  meh_t d = meh_init;

  file = fopen ("a", "r");
  fread (&c, sizeof (char), sizeof (meh_t), file);
  fread (&d, sizeof (char), sizeof (meh_t), file);
  fclose (file);

  printf ("C:%d, %s, %f\n", c.a, c.string, c.pi);
  printf ("D:%d, %s, %f\n", d.a, d.string, d.pi);

  return 0;
}
