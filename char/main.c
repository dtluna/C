#include <curses.h>
#include <unistd.h>

int
main (int argc, char const *argv[])
{
  initscr ();
  noecho ();
  keypad (stdscr, TRUE);
  printw ("Press a key:");
  sleep (10);
  char c = getch ();
  printw ("\nCharacter code: %d, '%c'.\n", c, c);
  printw ("Press any key to close this program.\n");
  getch ();
  endwin ();
  return 0;
}
