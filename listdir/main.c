#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#define NUM_OF_FILES 64

char **
allocate_string_array (size_t nmemb)
{
  char **string_array = (char **) calloc (nmemb, sizeof (char *));
  if (NULL == string_array)
    {
      perror ("Memory allocation error.");
      exit (EXIT_FAILURE);
    }
  return string_array;
}

char **
reallocate_string_array (char **string_array, size_t nmemb)
{
  string_array = (char **) realloc (string_array, nmemb);
  if (NULL == string_array)
    {
      perror ("Memory allocation error.");
      exit (EXIT_FAILURE);
    }
  return string_array;
}

int
main (int argc, char const *argv[])
{
  char *path;

  if (argc < 2)
    path = "./";
  if (argc == 2)
    path = argv[1];
  if (argc > 2)
    {
      fprintf (stderr,
	       "You are allowed to specify only one directory name by God.\n");
      exit (EXIT_FAILURE);
    }

  DIR *dp;
  struct dirent *ep;
  size_t num_of_files = NUM_OF_FILES;

  char **filenames = allocate_string_array (num_of_files);

  dp = opendir (path);
  if (NULL != dp)
    {
      size_t i = 0;
      int my_errno = errno;
      while (ep = readdir (dp))
	{
	  if (DT_REG == ep->d_type)
	    {
	      //puts(ep->d_name);
	      if (i == num_of_files)
		{
		  num_of_files *= 2;
		  filenames =
		    reallocate_string_array (filenames, num_of_files);
		}
	      filenames[i] = ep->d_name;
	      puts (filenames[i]);
	      i++;
	    }

	}
      if (errno != my_errno)
	{
	  perror ("readdir");
	  free (filenames);
	  exit (EXIT_FAILURE);
	}
      closedir (dp);
    }
  else
    {
      perror ("Couldn't open directory");
      free (filenames);
      exit (EXIT_FAILURE);
    }
  free (filenames);
  return 0;
}
