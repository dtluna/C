#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int
main ()
{
  printf ("Size of pid_t: %d.\n", sizeof (pid_t));
  return 0;
}
