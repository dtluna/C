#include <stdio.h>
#include <stdlib.h>

#ifndef remove_newline
#define remove_newline while('\n' == getchar()){continue;}
#endif

int
main (int argc, char const *argv[])
{
  int a = 10;
  char c = 'a';
  puts ("Enter integer:");
  scanf ("%d", &a);
  printf ("a = %d\n", a);
  getchar ();
  puts ("Enter char:");
  c = getchar ();
  printf ("c = '%c'\n", c);
  return 0;
}
