#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
main (int argc, char const *argv[])
{
  long l = 0;
  while (1)
    {
      printf ("%ld\n", l);
      for (int i = 1; i < argc; i++)
	{
	  printf ("%d:%s\n", getpid (), argv[i]);
	}
      l++;
      sleep (1);
    }
  return 0;
}
