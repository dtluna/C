#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void
pthread_check (int return_value, char *message)
{
  if (0 != return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}

void *
nyan (void *unused)
{
  pthread_mutex_lock (&mutex);
  puts ("Thread: Locked mutex. Waiting for condition.");
  pthread_cond_wait (&cond, &mutex);

  puts ("Nyan!");

  pthread_cond_signal (&cond);
  puts ("Thread: Signaled condition.");
  return 0;
}

int
main ()
{
  struct timespec waittime;
  waittime.tv_sec = 0;
  waittime.tv_nsec = 300;

  pthread_t thread;
  pthread_check (pthread_create (&thread, NULL, &nyan, NULL),
		 "pthread_create");
  puts ("Main: Created thread.");
  nanosleep (&waittime, NULL);

  pthread_cond_signal (&cond);
  puts ("Main: Signaled condition.");

  pthread_check (pthread_mutex_destroy (&mutex), "pthread_mutex_destroy");
  return 0;
}
