#include <stdio.h>
#include <stdlib.h>

#ifndef BY_ASCII
#define BY_ASCII '1'
#endif

#ifndef BY_LEN
#define BY_LEN '2'
#endif

void sort (char **names[], char attribute);

int
main (int argc, char const *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr,
	       "Не заданы строки для сортировки.\n");
      return 0;
    }

  char **names = (char **) calloc (argc - 1, sizeof (char *));
  for (int i = 0; i < argc - 1; ++i)
    names[i] = argv[i + 1];

  puts ("Выберите метод сортировки:");
  puts ("1 — по ASCII-кодам;");
  puts ("2 — по длине;");
  char c = getchar ();

  sort (&names, c);

  return 0;
}

void
sort (char ***names, char attribute)
{
  char *temp;

  switch (attribute)
    {
    case BY_ASCII:
      //сортируем по ASCII-кодам
      puts ("По ASCII-кодам.");
      break;

    case BY_LEN:
      //сортируем по длине
      puts ("По длине.");
      break;
    }
}
