#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <regex.h>

#ifndef ERR_BUF_SIZE
#define ERR_BUF_SIZE 256
#endif

void
regexp_handle_error (int errcode, regex_t * regexp)
{
  char *errbuf = (char *) calloc (ERR_BUF_SIZE, sizeof (char));
  regerror (errcode, regexp, errbuf, ERR_BUF_SIZE);
  fprintf (stderr, "%s\n", errbuf);
  regfree (regexp);
  exit (EXIT_FAILURE);
}

int
main (int argc, char **argv)
{
  if (argc < 3)
    {
      fprintf (stderr, "Usage:%s <regexp> <string> [strings]...\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  regex_t regexp;
  printf ("Regular expression:%s\n", argv[1]);

  puts ("Strings:");
  for (int i = 2; i < argc; ++i)
    puts (argv[i]);

  int error = regcomp (&regexp, argv[1], REG_NOSUB | REG_ICASE | REG_NEWLINE);
  if (0 != error)
    regexp_handle_error (error, &regexp);

  puts ("Matched strings:");
  for (int i = 2; i < argc; ++i)
    {
      error = regexec (&regexp, argv[i], 0, NULL, 0);
      if (0 == error)
	puts (argv[i]);
    }

  return 0;
}
