#include <stdio.h>
#include <stdlib.h>

#ifndef itoa
#define itoa(string, n) sprintf(string, "%d", n)
#endif

struct ring
{
  int n;
  char key;
  struct ring *next;
};

typedef struct ring ring_t;

void insert (ring_t ** start);
void show_ring (ring_t ** start);
void duplicate (ring_t ** start);

ring_t *ring_alloc (size_t nmemb);

int
main ()
{
  ring_t *start = NULL;

  do
    {
      insert (&start);
      fflush (stdin);
      puts ("Enter 'y' to continue.");

    }
  while (getchar () == 'y');

  puts ("Ring before duplicating:");
  show_ring (&(start)->next);

  duplicate (&start);

  puts ("Ring after duplicating:");
  show_ring (&(start)->next);

  return 0;
}

void
insert (ring_t ** start)
{
  ring_t *temp = ring_alloc (1);

  puts ("Enter number:");
  scanf ("%d", &temp->n);
  getchar ();
  /*чтобы чёртов newline достать.
     Если Луцик будет возникать, то скажи, что fflush(stdin) возникает неопределённое поведение */

  if (*start == NULL)
    {
      *start = temp;
      temp->key = 1;
      temp->next = temp;
    }
  else
    {
      temp->key = 0;
      temp->next = (*start)->next;
      (*start)->next = temp;
    }
}

void
show_ring (ring_t ** start)
{
  if ((*start)->key != 1)
    {
      printf ("%d ", (*start)->n);
      show_ring (&((*start))->next);
    }
  else
    {
      printf ("%d \n", (*start)->n);
      return;
    }
}

void
duplicate (ring_t ** start)
{
  ring_t *temp = ring_alloc (1), *left = ring_alloc (1), *right =
    ring_alloc (1);

  int num = 0;

  puts ("Enter the number to duplicate:");
  scanf ("%d", &num);
  getchar ();
  /*чтобы чёртов newline достать.
     Если Луцик будет возникать, то скажи, что fflush(stdin) возникает неопределённое поведение */

  left->key = right->key = 0;

  temp = *start;

  do
    {
      if (temp->next->n == num)
	{
	  left->n = right->n = num;

	  left->next = temp->next->next;
	  right->next = temp->next;
	  temp->next->next = left;
	  temp->next = right;
	  return;
	}
      else
	temp = temp->next;

    }
  while (temp->key != 1);

  puts ("You have entered the wrong number!");

}

ring_t *
ring_alloc (size_t nmemb)
{
  ring_t *ptr = (ring_t *) calloc (nmemb, sizeof (ring_t));
  if (NULL == ptr)
    {
      perror ("ring_alloc:calloc:");
      exit (EXIT_FAILURE);
    }
  return ptr;
}
