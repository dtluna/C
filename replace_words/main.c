#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef SYMBOL
#define INFILE_NAME "input.txt"
#endif

#ifndef OUTFILE_NAME
#define OUTFILE_NAME "output.txt"
#endif

#ifndef BUF_SIZE
#define BUF_SIZE 50
#endif

void fopen_err (FILE * ret_val);
void fclose_err (int ret_val);
size_t read_word (char *word, size_t size, FILE * infile);
void erase_spaces (char *string, size_t size);
size_t word_len (char *string, size_t size);
void get_back (FILE * infile, char *word, size_t bytes_read);

void
fopen_err (FILE * ret_val)
{
  if (NULL == ret_val)
    {
      perror ("fopen");
      exit (EXIT_FAILURE);
    }
}

void
fclose_err (int ret_val)
{
  if (EOF == ret_val)
    {
      perror ("fclose");
      exit (EXIT_FAILURE);
    }
}

void
fseek_err (int ret_val)
{
  if (-1 == ret_val)
    {
      perror ("fseek");
      exit (EXIT_FAILURE);
    }
}

void
erase_spaces (char *string, size_t size)
{
  int i = 0;
  while (' ' == string[i] && i < size)
    i++;

  int spaces_num = i;

  for (; i < size; ++i)
    string[i - spaces_num] = string[i];
}

size_t
word_len (char *string, size_t size)
{
  int i = 0;
  while (' ' != string[i] && '\0' != string[i] && '\n' != string[i]
	 && i < size)
    i++;

  return i;
}

size_t
read_word (char *word, size_t size, FILE * infile)
{
  size_t bytes_read = fread (word, sizeof (char), size, infile);

  int i = 0;

  erase_spaces (word, bytes_read);

  size_t word_length = word_len (word, bytes_read);

  for (i = word_length; i < bytes_read; ++i)
    word[i] = '\0';

  return bytes_read;
}

void
get_back (FILE * infile, char *word, size_t bytes_read)
{
  size_t word_length = word_len (word, bytes_read);
  fseek_err (fseek (infile, -1 * (BUF_SIZE - word_length - 1), SEEK_CUR));
}

int
main (int argc, char const *argv[])
{
  FILE *infile = fopen (INFILE_NAME, "r");
  fopen_err (infile);
  FILE *outfile = fopen (OUTFILE_NAME, "w");
  fopen_err (outfile);

  char buffer[BUF_SIZE] = "";
  size_t bytes_read = 0;

  /*
     do
     {
     bytes_read = read_word(buffer, BUF_SIZE, infile);    
     printf("%d word:'%s'\n", i, buffer);
     fprintf(outfile, "%s ", buffer);
     get_back(infile, buffer, bytes_read);

     if(ferror(infile))
     {
     clearerr(infile);
     }

     i++;
     //getchar();
     }while(bytes_read);
   */

  fclose_err (fclose (outfile));
  fclose_err (fclose (infile));

  return 0;
}
