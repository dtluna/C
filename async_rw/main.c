#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <aio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>

#define BUF_SIZE 4096

void thread_check (int return_value, char *message);
void create_thread (pthread_t * thread,
		    const pthread_attr_t * attr,
		    void *(*start_routine) (void *), void *arg);

void file_check (FILE * return_value, char *message);

sem_t *open_semaphore (const char *name, int oflag,
		       mode_t mode, unsigned int value);
void semaphore_check (int return_value, char *func_name);
void semaphore_post (sem_t * semaphore);

void *read_func (struct aiocb *srccb_ptr);
void *write_func (struct aiocb *destcb_ptr);

int
main (int argc, char const *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage:%s <source> <destination>\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  struct aiocb srccb;
  struct aiocb destcb;

  FILE *source = fopen (argv[1], "r");
  file_check (source, "fopen(r)");
  FILE *destination = fopen (argv[2], "w");
  file_check (destination, "fopen(w)");

  char buffer[BUF_SIZE];

  srccb.aio_fildes = fileno (source);
  destcb.aio_fildes = fileno (destination);
  srccb.aio_reqprio = destcb.aio_reqprio = 20;

  srccb.aio_offset = destcb.aio_offset = 0;
  srccb.aio_buf = destcb.aio_buf = buffer;
  srccb.aio_nbytes = destcb.aio_nbytes = sizeof (buffer);
  srccb.aio_sigevent.sigev_notify = destcb.aio_sigevent.sigev_notify =
    SIGEV_NONE;

  pthread_t thread;

  create_thread (&thread, NULL, &read_func, &srccb);
  write_func (&destcb);

  if (strcmp (srccb.aio_buf, destcb.aio_buf))
    puts ("srccb.aio_buf != destcb.aio_buf");
  else
    puts ("srccb.aio_buf == destcb.aio_buf");

  return 0;
}

sem_t *
open_semaphore (const char *name, int oflag, mode_t mode, unsigned int value)
{
  sem_t *semaphore = sem_open (name, oflag, mode, value);
  if (SEM_FAILED == semaphore)
    {
      fprintf (stderr, "%s:", name);
      perror ("sem_open");
      exit (EXIT_FAILURE);
    }
  return semaphore;
}

void
semaphore_check (int return_value, char *func_name)
{
  if (0 != return_value)
    {
      perror (func_name);
      exit (EXIT_FAILURE);
    }
}

void
semaphore_post (sem_t * semaphore)
{
  semaphore_check (sem_post (semaphore), "sem_post");
}

void
semaphore_wait (sem_t * semaphore)
{
  semaphore_check (sem_wait (semaphore), "sem_wait");
}

void
thread_check (int return_value, char *message)
{
  if (0 != return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}

void
create_thread (pthread_t * thread,
	       const pthread_attr_t * attr,
	       void *(*start_routine) (void *), void *arg)
{
  thread_check (pthread_create (thread, attr, start_routine, arg),
		"pthread_create");
}

void
file_check (FILE * return_value, char *message)
{
  if (NULL == return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}

void *
read_func (struct aiocb *srccb_ptr)
{
  sem_t *finished_reading =
    open_semaphore ("/finished_reading", O_CREAT, 0666, 0);

  aio_read (srccb_ptr);
  int error = 0;
  while (error = aio_error (srccb_ptr))
    {
      if (EINPROGRESS == error)
	continue;
      if (ECANCELED == error)
	{
	  fprintf (stderr, "The request was cancelled.\n");
	  exit (EXIT_FAILURE);
	}
      if (error > 0)
	{
	  perror ("aio_read");
	  exit (EXIT_FAILURE);
	}
    }
  printf ("strlen(srccb_ptr->aio_buf) = %d\n", strlen (srccb_ptr->aio_buf));
  puts (srccb_ptr->aio_buf);
  semaphore_post (finished_reading);
  return NULL;
}

void *
write_func (struct aiocb *destcb_ptr)
{
  sem_t *finished_reading =
    open_semaphore ("/finished_reading", O_CREAT, 0666, 0);
  semaphore_wait (finished_reading);

  destcb_ptr->aio_nbytes = strlen (destcb_ptr->aio_buf);

  aio_write (destcb_ptr);
  int error = 0;
  while (error = aio_error (destcb_ptr))
    {
      if (EINPROGRESS == error)
	continue;
      if (ECANCELED == error)
	{
	  fprintf (stderr, "The request was cancelled.\n");
	  exit (EXIT_FAILURE);
	}
      if (error > 0)
	{
	  perror ("aio_write");
	  exit (EXIT_FAILURE);
	}
    }
  printf ("strlen(destcb_ptr->aio_buf) = %d\n", strlen (destcb_ptr->aio_buf));
  puts (destcb_ptr->aio_buf);
  return NULL;
}
