#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int
main ()
{
  char *error;
  void *handle = dlopen ("./functions.so", RTLD_LAZY);
  if (!handle)
    {
      fprintf (stderr, "%s\n", dlerror ());
      exit (EXIT_FAILURE);
    }

  void (*hello) (void) = dlsym (handle, "hello");
  if ((error = dlerror ()) != NULL)
    {
      fprintf (stderr, "%s\n", error);
      exit (EXIT_FAILURE);
    }
  void (*put_cos) (void) = dlsym (handle, "put_cos");
  if ((error = dlerror ()) != NULL)
    {
      fprintf (stderr, "%s\n", error);
      exit (EXIT_FAILURE);
    }

  hello ();
  put_cos ();

  return 0;
}
