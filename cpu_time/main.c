#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int
main (int argc, char const *argv[])
{
  if (argc < 2)
    {
      fprintf (stderr, "Usage:%s <number of nanoseconds to sleep>\n",
	       argv[0]);
      exit (EXIT_FAILURE);
    }

  FILE *cpu_stat_file = NULL;
  FILE *uptime_file = NULL;
  clock_t user = 0, nice = 0, system_time = 0, idle = 0, iowait = 0;
  clock_t irq = 0, softirq = 0, steal = 0, guest = 0, guest_nice = 0;
  time_t cpu_time = 0;
  time_t uptime = 0;

  struct timespec ts;
  ts.tv_sec = 0;
  ts.tv_nsec = atoll (argv[1]);

  while (1)
    {
      cpu_stat_file = fopen ("/proc/stat", "r");
      fscanf (cpu_stat_file,
	      "%*s %lu %lu %lu %lu %lu"
	      "%lu %lu %lu %lu %lu",
	      &user, &nice, &system_time, &idle, &iowait,
	      &irq, &softirq, &steal, &guest, &guest_nice);
      fclose (cpu_stat_file);

      uptime_file = fopen ("/proc/uptime", "r");
      fscanf (uptime_file, "%lu", &uptime);
      fclose (uptime_file);

      cpu_time =
	(user + nice + system_time + idle + iowait + irq + softirq + steal +
	 guest + guest_nice) / sysconf (_SC_CLK_TCK);
      printf ("CPU time = %lu.\n", cpu_time);
      printf ("Uptime = %lu.\n", uptime);
      //printf("CPU time/Uptime = %lf\n", 1.0*cpu_time/uptime);
      nanosleep (&ts, NULL);
    }

  return 0;
}
