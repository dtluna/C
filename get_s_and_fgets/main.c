#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main ()
{
  char fgets_string[256] = "";
  char get_s_string[256] = "";

  puts ("Enter a string which will be read by fgets():");
  fgets (fgets_string, 256, stdin);

  puts ("Enter a string which will be read by gets_s():");
  gets_s (get_s_string, 256);

  if (0 == strcmp (fgets_string, get_s_string))
    puts ("Strings are equal.");
  else
    puts ("Strings are not equal.");

  return 0;
}
