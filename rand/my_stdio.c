#include "my_stdio.h"
#include <stdlib.h>
#include "errors.h"
#include <string.h>

FILE *
file_open (const char *path, const char *mode)
{
  FILE *file = fopen (path, mode);
  fopen_err (file);
  return file;
}

void
file_close (FILE * file)
{
  fclose_err (fclose (file));
}

void
file_seek (FILE * stream, long offset, int whence)
{
  fseek_err (fseek (stream, offset, whence));
}

void
puterrs (const char *s)
{
  fprintf (stderr, "%s\n", s);
}

void
get_string (char *string, int size)
{
  fgets_err (fgets (string, size, stdin));
}

void
remove_newline (char *string)
{
  int newline_pos = strlen (string) - 1;
  if ('\n' == string[newline_pos])
    string[newline_pos] = '\0';
}
