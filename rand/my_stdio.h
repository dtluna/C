#include <stdio.h>
FILE* file_open(const char *path, const char *mode);
void file_close(FILE* file);
void file_seek(FILE *stream, long offset, int whence);
void puterrs (const char *s);
void get_string(char* string, int size);
void remove_newline(char* string);
