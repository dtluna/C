#include <stdlib.h>
#include "my_stdio.h"

int
main (int argc, char const *argv[])
{
  FILE *dev_urandom = file_open ("/dev/urandom", "rb");

  unsigned int random = 0;
  fread (&random, sizeof (random), 1, dev_urandom);
  file_close (dev_urandom);

  srand (random);
  printf ("%d\n", rand ());
  return 0;
}
