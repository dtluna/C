#include <stdlib.h>
#include <stdio.h>
#include <argp.h>
#include <err.h>

const char *argp_program_version = "argp-example 1.0";
const char *argp_program_bug_address = "<nikitin@openmailbox.org>";

/* Program documentation. */
static char doc[] =
  "Argp example — a program with options and arguments using argp.";

/* A description of the arguments we accept. */
static char args_doc[] = "ARG1 ARG2";

/* The options we understand. */
static struct argp_option options[] = {
  {"mult", 'm', 0, 0, "Multiply arguments on each other"},
  {0}
};

/* Used by main to communicate with parse_opt. */
struct arguments
{
  char *args[2];		/* arg1 & arg2 */
  char mult;
};

static error_t
parser (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'm':
      arguments->mult = 1;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 2)
        /* Too many arguments. */
        argp_usage (state);

      arguments->args[state->arg_num] = arg;

      break;

    case ARGP_KEY_END:
      if (state->arg_num < 2)
        /* Not enough arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parser, args_doc, doc };

long double
to_long_double (const char *str)
{
  long double n = 0;
  int res = sscanf (str, "%Lg", &n);
  if (1 > res)
    {
      errx (1, "%s is not a double", str);
    }
  return n;
}

int
main (int argc, char **argv)
{
  struct arguments args;

  /* Default values. */
  args.mult = 0;

  /* Parse our arguments; every option seen by parser() will
     be reflected in arguments. */
  argp_parse (&argp, argc, argv, 0, 0, &args);
  long double arg1 = to_long_double (args.args[0]);
  long double arg2 = to_long_double (args.args[1]);
  printf ("ARG1:%Lg\nARG2:%Lg\nmult:%s\n",
          arg1, arg2, args.mult ? "yes" : "no");
  if (args.mult)
    {
      printf("Result: %Lg\n", arg1 * arg2);
    }
  else
    {
      printf("Result: %Lg\n", arg1 + arg2);
    }
  exit (0);
}
