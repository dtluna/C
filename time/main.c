#include <cdk/cdk.h>
#include <locale.h>
#include <signal.h>

void quit (int signal, siginfo_t * not_needed, void *unused);

int
main (int argc, char const *argv[])
{
  WINDOW *screen;
  screen = initscr ();

  setlocale (LC_ALL, "en_US.utf8");

  CDKSCREEN *main_scr = initCDKScreen (screen);
  initCDKColor ();

  struct sigaction handler;
  handler.sa_sigaction = &(quit);

  sigaction (SIGINT, &handler, NULL);

  time_t time_since_Epoch = 0;
  time_since_Epoch = time (NULL);
  char *local_time = ctime (&time_since_Epoch);

  CDKLABEL *clock_label =
    newCDKLabel (main_scr, CENTER, CENTER, &local_time, 1, TRUE, FALSE);
  while (1)
    {
      time_since_Epoch = time (NULL);
      local_time = ctime (&time_since_Epoch);
      setCDKLabel (clock_label, &local_time, 1, TRUE);
      drawCDKLabel (clock_label, TRUE);
      sleep (1);
    }
  endCDK ();
  exit (0);
}

void
quit (int signal, siginfo_t * not_needed, void *unused)
{
  endCDK ();
  exit (0);
}
