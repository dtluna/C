#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define SIZE 256

int
main ()
{
  char filename[SIZE];
  time_t curtime = time (NULL);
  struct tm *loctime = localtime (&curtime);
  strftime (filename, SIZE, "%d.%m.%y_%I.%M.%S.xml", loctime);
  FILE *file = fopen ("temp.xml", "w");
  if (NULL == file)
    {
      perror ("fopen");
      exit (EXIT_FAILURE);
    }
  malloc_info (0, file);
  fclose (file);
  char call_string[SIZE];
  sprintf (call_string, "xmlformat temp.xml > %s", filename);
  system (call_string);
  system ("rm temp.xml");
  return 0;
}
