#include <stdio.h>

void malloc_err(void* ret_val);//NULL
void fopen_err(FILE* ret_val);//NULL & errno
void fclose_err(int ret_val);//EOF & errno
void fseek_err(int ret_val); //-1 & errno
void fgets_err(char* ret_val); //NULL
void scandir_err(int ret_val); //-1
