#include <sys/types.h>

typedef char* string_t;

string_t* alloc_str_array (size_t nmemb);
string_t alloc_str (size_t length);
time_t* alloc_time_t_array (size_t nmemb);
