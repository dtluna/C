#include <dirent.h>

typedef char* string_t;

static const string_t file_type_strings[8] = {"Block devices", "Character devices",
"Directories", "FIFOs", "Symbolic links", "Regular files",
"UNIX domain sockets", "Unknown type files"};

int scan_directory(const char *dirp, struct dirent ***namelist,
			  int (*filter)(const struct dirent *),
			  int (*compar)(const struct dirent **, const struct dirent **));
int blk_filter(const struct dirent *de);
int chr_filter(const struct dirent *de);
int dir_filter(const struct dirent *de);
int fifo_filter(const struct dirent *de);
int lnk_filter(const struct dirent *de);
int reg_filter(const struct dirent *de);
int sock_filter(const struct dirent *de);
int unknown_filter(const struct dirent *de);

static int (*filters[8])(const struct dirent *) = {blk_filter, chr_filter, dir_filter, fifo_filter, 
lnk_filter, reg_filter, sock_filter, unknown_filter};
