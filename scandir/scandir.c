#include "scandir.h"
#include "errors.h"

int
scan_directory (const char *dirp, struct dirent ***namelist,
		int (*filter) (const struct dirent *),
		int (*compar) (const struct dirent **,
			       const struct dirent **))
{
  int entries_num = scandir (dirp, namelist, filter, compar);
  scandir_err (entries_num);
  return entries_num;
}

int
blk_filter (const struct dirent *de)
{
  if (DT_BLK == de->d_type)
    return 1;
  else
    return 0;
}

int
chr_filter (const struct dirent *de)
{
  if (DT_CHR == de->d_type)
    return 1;
  else
    return 0;
}

int
dir_filter (const struct dirent *de)
{
  if (DT_DIR == de->d_type)
    return 1;
  else
    return 0;
}

int
fifo_filter (const struct dirent *de)
{
  if (DT_FIFO == de->d_type)
    return 1;
  else
    return 0;
}

int
lnk_filter (const struct dirent *de)
{
  if (DT_LNK == de->d_type)
    return 1;
  else
    return 0;
}

int
reg_filter (const struct dirent *de)
{
  if (DT_REG == de->d_type)
    return 1;
  else
    return 0;
}

int
sock_filter (const struct dirent *de)
{
  if (DT_SOCK == de->d_type)
    return 1;
  else
    return 0;
}

int
unknown_filter (const struct dirent *de)
{
  if (DT_UNKNOWN == de->d_type)
    return 1;
  else
    return 0;
}
