#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "scandir.h"

/*
 выведи мне все имена файлов из папки, которую задаешь в командной строке.
 Выведи отдельно директории и обычные файлы.
*/

int
main (int argc, char const *argv[])
{
  if (argc != 2)
    {
      fprintf (stderr, "Usage:%s directory\n", argv[0]);
      exit (-1);
    }

  struct dirent **files[8] = {[0 ... 7] = NULL };

  int files_num[8] = { 0 };
  for (int i = 0; i < 8; ++i)
    files_num[i] =
      scan_directory (argv[1], &files[i], filters[i], &alphasort);

  for (int i = 0; i < 8; ++i)
    {
      if (0 == files_num[i])
	continue;

      printf ("%s:\n", file_type_strings[i]);

      for (int j = 0; j < files_num[i]; ++j)
	printf ("\t%s\n", files[i][j]->d_name);
    }

  return 0;
}
