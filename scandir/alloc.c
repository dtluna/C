#include "alloc.h"
#include "errors.h"
#include <stdlib.h>

string_t *
alloc_str_array (size_t nmemb)
{
  char **str_array = (char **) calloc (nmemb, sizeof (char *));
  malloc_err (str_array);
  return str_array;
}

string_t
alloc_str (size_t length)
{
  string_t string = (string_t) calloc (length, sizeof (char));
  malloc_err (string);
  return string;
}
