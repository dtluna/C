#include <stdio.h>
#include <stdlib.h>

void
print_array (short array[])
{
  for (int i = 0; i < 10; i++)
    {
      printf ("%3d", array[i]);
    }
  puts (";");
}

int
main ()
{
  short i = 10;
  short a[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  short b[10] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
  short c[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  printf ("a =");
  print_array (a);
  printf ("b =");
  print_array (b);
  printf ("c =");
  print_array (c);

  char name[] = "thisisatest\0";
  __asm__ volatile ("incb (%0)\n\t" "incw (%1)\n\t"::"r" (name), "r" (a):"memory");	// AT&T syntax
  printf ("%s\n", name);
  printf ("a =");
  print_array (a);
	/*__asm__ ("loop1:\n\t"
		"dec %0+%1\n\t"
		"jnz loop1\n\t"
		: "=r" (*c)
		: "m"(i)"o"(a), "o" (b),  );
		*/
  printf ("i = %d\n", i);
  return 0;
}
