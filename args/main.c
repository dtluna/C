#include <stdio.h>

struct return_vals
{
  int i;
  float f;
  char c;
};

struct return_vals
fun ()
{
  struct return_vals vals;
  vals.i = 1337;
  vals.f = 3.14;
  vals.c = 'h';

  return vals;
}

void
blah (struct return_vals *vals)
{
  vals->i = 1488;
  vals->f = 2.7;
  vals->c = 'd';
}

void
nyan (int *i, float *f)
{
  *i = 265;
  *f = 1.618;
}

int
main (int argc, char const *argv[])
{
  struct return_vals vals = fun ();
  printf ("i:%d, f:%f, c:%c\n", vals.i, vals.f, vals.c);
  blah (&vals);
  printf ("i:%d, f:%f, c:%c\n", vals.i, vals.f, vals.c);

  int i = 0;
  float f = 0;

  nyan (&i, &f);
  printf ("i:%d, f:%f\n", i, f);

  return 0;
}
