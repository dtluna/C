#include <stdio.h>

int
main ()
{
  int sum = 0, x = 1, y = 2;
__asm ("addl %2, %1\n\t" "movl %%eax, %0\n\t": "=r" (sum):"ra" (x),
	 "rb" (y));
  // x — в ax, а y — в bx
  printf ("sum = %d, x = %d, y = %d\n", sum, x, y);	// sum = 3, x = 1, y = 2
  return 0;
}
