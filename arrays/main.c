#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

short a[SIZE] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
short b[SIZE] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
short c[SIZE] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

void
print_array (short array[SIZE])
{
  for (int i = 0; i < SIZE; i++)
    printf ("%2d", array[i]);
  printf (";\n");
}

int
main ()
{
  puts ("Before assembly:");
  printf ("'a' array:");
  print_array (a);
  printf ("'b' array:");
  print_array (b);
  printf ("'c' array:");
  print_array (c);
  //section:[base + index*scale + disp], which changes to

  //section:disp(base, index, scale) in AT&T. 
__asm ("push %%rsi\n\t" "\n\t" "\n\t" "pop %%rsi\n\t":
:"a" (a), "b" (b)		//в ax — адрес а, в bx — b
    );

  puts ("After assembly:");
  printf ("'a' array:");
  print_array (a);
  printf ("'b' array:");
  print_array (b);
  printf ("'c' array:");
  print_array (c);

  return 0;
}
