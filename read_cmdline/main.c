#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void check_error (int return_value, int error_value, char *message);

#define NULL_error(e, msg) check_error(e, NULL, msg);
#define fopen_err(e) NULL_error(e, "fopen");
#define malloc_err(e) NULL_error(e,"malloc");
#define fgets_err(e) NULL_error(e,"fgets");

#define minus_one_error(e, msg) check_error(e, -1, msg);
#define chdir_err(e) minus_one_error(e,"chdir");
#define scandir_err(e) minus_one_error(e,"scandir");
#define stat_err(e) minus_one_error(e, "stat");

int
main ()
{
  chdir_err (chdir ("/proc/self/"));
  FILE *cmdline_file = fopen ("./cmdline", "r");
  fopen_err (cmdline_file);
  size_t max_cmdline_len = 0;
  max_cmdline_len = 4096;
  char *buffer = (char *) calloc (max_cmdline_len, sizeof (char));
  malloc_err (buffer);
  if (EOF == fscanf (cmdline_file, "%s", buffer))
    {
      perror ("fscanf");
      ferror (cmdline_file);
    }
  for (size_t i = 0; i < max_cmdline_len - 2; ++i)
    if ('\0' == buffer[i] && '\0' != buffer[i + 1])
      buffer[i] = ' ';
  puts (buffer);
  return 0;
}

void
check_error (int return_value, int error_value, char *message)
{
  if (error_value == return_value)
    {
      perror (message);
      exit (EXIT_FAILURE);
    }
}
