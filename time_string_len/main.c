#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int
main (int argc, char const *argv[])
{
  size_t prev_time_str_len = 0;
  printf ("time_t size = %lu\n", sizeof (time_t));
  fflush (stdout);

  for (time_t i = 0; i < 4611686018427387904; ++i)
    {
      if (prev_time_str_len != strlen (ctime (&i)))
	{
	  printf ("String length changed! i = %lu, length = %lu \n", i,
		  strlen (ctime (&i)));
	  fflush (stdout);
	}


      prev_time_str_len = strlen (ctime (&i));
    }
  return 0;
}
