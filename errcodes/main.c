#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)


int
main (int argc, const char **argv)
{
  errno = 0;
  for (int i = 0; i < 134; ++i)	/*опытным путём установленное число. 
				   Не бойстесь подкрутить вверх и посмотреть, что будет */
    puts (strerror (i));
  return 0;
}
